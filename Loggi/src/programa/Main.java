package programa;

/*
Nome: Ulremberg
Universidade: UFRPE
Curso: Licenciatura em Computação
Semestre atual: 5
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class Main {

	static int indexPacote = 0;
	static int somaRegiaoSul = 0;
	static int somaRegiaoSudeste = 0;
	static int somaRegiaOeste = 0;
	static int somaRegiaoNordeste = 0;
	static int somaRegiaoNorte = 0;
	static int somaVendedorRegiaoSul = 0;
	static int somaVendedorRegiaoSudeste = 0;
	static int somaVendedorRegiaOeste = 0;
	static int somaVendedorRegiaoNordeste = 0;
	static int somaVendedorRegiaoNorte = 0;

	static List<String> listaPacoteOrigemSulBrinquedo = new ArrayList<>();
	static Map<String, String> listaPacoteDestino = new HashMap<>();
	static Map<String, Integer> listaRegiaoDestinoTotal = new HashMap<>();
	static Set<Integer> pacotesCodigosInvalidos = new HashSet<>();
	static Set<Integer> pacotesCodigosValidos = new HashSet<>();
	static Map<String, Integer> listaEnviadoVendedor = new HashMap<>();
	static Map<String, Set<String>> listaDestinoTipo = new HashMap<>();
	static Map<Integer, String> listaTipoProduto = new HashMap<>();
	static List<String> pacotesNorteECentroOeste = new ArrayList<>();
	static Queue<String> cargaNorte = new LinkedList<>();
	static Queue<String> descarregarCentroOeste = new LinkedList<>();
	static Queue<String> cargaNorteJoias = new LinkedList<>();
	static Queue<String> descarregarCentroOesteJoias = new LinkedList<>();
	static List<String> produtoInvalido = new ArrayList<>();

	public static void main(String[] args) throws FileNotFoundException {

		File fileText = new File("dados.txt");

		Scanner sc = new Scanner(fileText);

		listaTipoProduto.put(001, "Joias");
		listaTipoProduto.put(111, "Livros");
		listaTipoProduto.put(333, "Eletronicos");
		listaTipoProduto.put(555, "Bebidas");
		listaTipoProduto.put(888, "Brinquedos");

		while (sc.hasNext()) {
			indexPacote++;
			String lida = "";
			String origemLida = "";
			String destinoLido = "";
			String codigoLido = "";
			String vendedorLido = "";
			String produtoLido = "";
			
			lida = sc.next();
			origemLida = lida.substring(0, 3);
			destinoLido = lida.substring(3, 6);
			codigoLido = lida.substring(6, 9);
			vendedorLido = lida.substring(9, 12);
			produtoLido = lida.substring(12, 15);			
			valida(origemLida, destinoLido, codigoLido, vendedorLido, produtoLido, lida);

		}

		System.out.println("1 - Regioes e pacotes " + listaRegiaoDestinoTotal);
		System.out.println();
		System.out.println("2 - Pacote(s) codigos invalido(s): " + pacotesCodigosInvalidos + "\nPacote(s) codigos valido(s): " + pacotesCodigosValidos);
		System.out.println();
		System.out.println("3 - Pacote(s) Sul e com Brinquedo " + listaPacoteOrigemSulBrinquedo);
		System.out.println();
		System.out.println("4 - Pacotes e Regioes " + listaPacoteDestino);
		System.out.println();
		System.out.println("5 - Vendedores: " + listaEnviadoVendedor);
		System.out.println();
		System.out.println("6 - Destino e tipo: " + listaDestinoTipo);
		System.out.println();
		System.out.println("7 - Pacotes no mesmo caminhao " + pacotesNorteECentroOeste);
		System.out.println();
		System.out.println("8 - Pacotes a serem carregados" + cargaNorte + "\nPacotes a serem descarregados " + descarregarCentroOeste);
		System.out.println();
		System.out.println("9 - Pacotes a serem carregados" + cargaNorteJoias + "\nPacotes a serem descarregados " + descarregarCentroOesteJoias);
		System.out.println();
		System.out.println("10 - Pacotes(s) invalido(s):" + pacotesCodigosInvalidos);

		sc.close();
	}

	private static void valida(String origemLida, String destinoLido, String codigoLido, String vendedorLido,
			String produtoLido, String lida) {
		boolean isvalidaRegiao = validaRegiao(origemLida, destinoLido, lida);
		boolean isValidaJoaiOeste = false;
		boolean isValidaVendedor = false;
		if (isvalidaRegiao) {
			isValidaJoaiOeste = validaJoaiOeste(origemLida, produtoLido, lida);
		}

		if (isValidaJoaiOeste) {
			isValidaVendedor = validaVendedor(vendedorLido, lida);

		}

		if (isValidaVendedor) {
			pacotesCodigosValidos.add(indexPacote);
			pacoteEnviadoPorVendedor(vendedorLido);
			regiaoDestinoPacote(destinoLido);
			pacoteDestinoTipo(destinoLido, produtoLido);
			pacotesNorteECentroOeste(destinoLido, lida);
			pacoteCargaNorteEDescarga(origemLida, destinoLido, lida);
			pacoteCargaNorteEDescargaJoias(origemLida, destinoLido, produtoLido, lida);
			pacotePorDestino(destinoLido, lida);
			pacoteOrigemSulBrinquedo(origemLida, produtoLido, lida);
			produtoInvalido(produtoLido, lida);

		}

	}

	private static void produtoInvalido(String produtoLido, String lida) {
		int produtoLidoValor = Integer.parseInt(produtoLido);
		if (listaTipoProduto.containsKey(produtoLidoValor)) {
			
		}
		else {
			produtoInvalido.add("Pacote " + indexPacote + " codigo: " + lida);
		}
	}

	private static void pacoteOrigemSulBrinquedo(String origemLida, String produtoLido, String lida) {
		int origemLidaValor = Integer.parseInt(origemLida);
		int produtoLidoValor = Integer.parseInt(produtoLido);

		if (origemLidaValor >= 100 && origemLidaValor <= 199) {
			if (listaTipoProduto.containsKey(produtoLidoValor)
					&& listaTipoProduto.get(produtoLidoValor).equals("Brinquedos")) {
				listaPacoteOrigemSulBrinquedo.add(lida);

			}

		}

	}

	private static void pacotePorDestino(String destinoLido, String lida) {
		listaPacoteDestino.put(lida, destinoLido);

		int destinoLidoValor = Integer.parseInt(destinoLido);

		if (destinoLidoValor >= 1 && destinoLidoValor <= 99) {
			listaPacoteDestino.put(lida, "Sudeste");

		}

		else if (destinoLidoValor >= 100 && destinoLidoValor <= 199) {
			listaPacoteDestino.put(lida, "Sul");
		}

		else if (destinoLidoValor >= 201 && destinoLidoValor <= 299) {
			listaPacoteDestino.put(lida, "Oeste");

		} else if (destinoLidoValor >= 300 && destinoLidoValor <= 399) {
			listaPacoteDestino.put(lida, "Nordeste");
		} else if (destinoLidoValor >= 400 && destinoLidoValor <= 499) {
			listaPacoteDestino.put(lida, "Norte");

		}

	}

	private static void pacoteCargaNorteEDescargaJoias(String origemLida, String destinoLido, String produtoLido,
			String lida) {
		int destinoLidoValor = Integer.parseInt(destinoLido);
		int origemLidaValor = Integer.parseInt(origemLida);
		if ((destinoLidoValor >= 201 && destinoLidoValor <= 299)
				|| (origemLidaValor >= 400 && origemLidaValor <= 499)) {
			if (destinoLidoValor >= 201 && destinoLidoValor <= 299) {
				descarregarCentroOesteJoias.add("Pacote " + indexPacote + " codigo: " + lida);
			}

			if (origemLidaValor >= 400 && origemLidaValor <= 499) {
				if (produtoLido.equals("Joia"))
					cargaNorteJoias.add("Pacote " + indexPacote + " codigo: " + lida);
			}
		}

	}

	private static void pacoteCargaNorteEDescarga(String origemLida, String destinoLido, String lida) {
		int destinoLidoValor = Integer.parseInt(destinoLido);
		int origemLidaValor = Integer.parseInt(origemLida);
		if ((destinoLidoValor >= 201 && destinoLidoValor <= 299)
				|| (origemLidaValor >= 400 && origemLidaValor <= 499)) {
			if (destinoLidoValor >= 201 && destinoLidoValor <= 299) {
				descarregarCentroOeste.add("Pacote " + indexPacote + " codigo: " + lida);
			}

			if (origemLidaValor >= 400 && origemLidaValor <= 499) {
				cargaNorte.add("Pacote " + indexPacote + " codigo: " + lida);
			}
		}

	}

	private static void pacotesNorteECentroOeste(String destinoLido, String lida) {

		int destinoLidoValor = Integer.parseInt(destinoLido);
		if ((destinoLidoValor >= 201 && destinoLidoValor <= 299)
				|| (destinoLidoValor >= 400 && destinoLidoValor <= 499)) {
			pacotesNorteECentroOeste.add("Pacote " + indexPacote + " codigo: " + lida);
		}

	}

	private static void pacoteDestinoTipo(String destinoLido, String produtoLido) {

		int destinoLidoValor = Integer.parseInt(destinoLido);
		int produtoLidoValor = Integer.parseInt(produtoLido);

		if (destinoLidoValor >= 1 && destinoLidoValor <= 99) {
			if (listaTipoProduto.containsKey(produtoLidoValor)) {

				listaDestinoTipo.computeIfAbsent("Sudeste", k -> new HashSet<>())
						.add(listaTipoProduto.get(produtoLidoValor));

			}

		}

		else if (destinoLidoValor >= 100 && destinoLidoValor <= 199) {
			if (listaTipoProduto.containsKey(produtoLidoValor))
				listaDestinoTipo.computeIfAbsent("Sul", k -> new HashSet<>())
						.add(listaTipoProduto.get(produtoLidoValor));
		} else if (destinoLidoValor >= 201 && destinoLidoValor <= 299) {
			if (listaTipoProduto.containsKey(produtoLidoValor)) {
				listaDestinoTipo.computeIfAbsent("Oeste", k -> new HashSet<>())
						.add(listaTipoProduto.get(produtoLidoValor));
			}

		} else if (destinoLidoValor >= 300 && destinoLidoValor <= 399) {
			if (listaTipoProduto.containsKey(produtoLidoValor)) {
				listaDestinoTipo.computeIfAbsent("Nordeste", k -> new HashSet<>())
						.add(listaTipoProduto.get(produtoLidoValor));

			}

		} else if (destinoLidoValor >= 400 && destinoLidoValor <= 499) {

			if (listaTipoProduto.containsKey(produtoLidoValor)) {

				listaDestinoTipo.computeIfAbsent("Norte", k -> new HashSet<>())
						.add(listaTipoProduto.get(produtoLidoValor));
			}

		}

	}

	private static boolean validaVendedor(String vendedorLido, String lida) {
		int vendedorLidoValor = Integer.parseInt(vendedorLido);
		if (vendedorLidoValor == 367) {
			pacotesCodigosInvalidos.add(indexPacote);
			return false;
		}

		return true;

	}

	private static void pacoteEnviadoPorVendedor(String vendedorLido) {

		int contador = listaEnviadoVendedor.containsKey(vendedorLido) ? listaEnviadoVendedor.get(vendedorLido) : 0;
		listaEnviadoVendedor.put(vendedorLido, contador + 1);

	}

	private static boolean validaJoaiOeste(String origemLida, String produtoLido, String lida) {
		int origemLidaValor = Integer.parseInt(origemLida);
		int produtoLidoValor = Integer.parseInt(produtoLido);
		if (produtoLidoValor == 001 && (origemLidaValor >= 201 && origemLidaValor <= 299)) {
			pacotesCodigosInvalidos.add(indexPacote);
			return false;
			
		}
		return true;

	}

	private static boolean validaRegiao(String origemLida, String destinoLido, String lida) {
		int origemLidaValor = Integer.parseInt(origemLida);
		int destinoLidoValor = Integer.parseInt(destinoLido);

		if ((origemLidaValor == 000 || origemLidaValor > 499 || origemLidaValor == 200)
				|| (destinoLidoValor == 000 || destinoLidoValor > 499) || destinoLidoValor == 200) {
			pacotesCodigosInvalidos.add(indexPacote);
			
			return false;
		}

		return true;

	}

	private static void regiaoDestinoPacote(String destinoLido) {
		int destinoLidoValor = Integer.parseInt(destinoLido);

		if (destinoLidoValor >= 1 && destinoLidoValor <= 99) {
			somaRegiaoSudeste++;
			listaRegiaoDestinoTotal.put("Sudeste", somaRegiaoSudeste);
		} else if (destinoLidoValor >= 100 && destinoLidoValor <= 199) {
			somaRegiaoSul++;
			listaRegiaoDestinoTotal.put("Sul", somaRegiaoSul);
		} else if (destinoLidoValor >= 201 && destinoLidoValor <= 299) {
			somaRegiaOeste++;
			listaRegiaoDestinoTotal.put("Oeste", somaRegiaOeste);
		} else if (destinoLidoValor >= 300 && destinoLidoValor <= 399) {
			somaRegiaoNordeste++;
			listaRegiaoDestinoTotal.put("Nordeste", somaRegiaoNordeste);
		} else if (destinoLidoValor >= 400 && destinoLidoValor <= 499) {
			somaRegiaoNorte++;
			listaRegiaoDestinoTotal.put("Norte", somaRegiaoNorte);
		}

		

	}

}
